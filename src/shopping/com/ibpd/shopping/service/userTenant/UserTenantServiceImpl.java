package com.ibpd.shopping.service.userTenant;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.shopping.entity.TenantEntity;
import com.ibpd.shopping.entity.UserTenantEntity;
import com.ibpd.shopping.service.tenant.ITenantService;
import com.ibpd.shopping.service.tenant.TenantServiceImpl;
@Service("userTenantService")
public class UserTenantServiceImpl extends BaseServiceImpl<UserTenantEntity> implements IUserTenantService {
	public UserTenantServiceImpl(){
		super();
		this.tableName="UserTenantEntity";
		this.currentClass=UserTenantEntity.class;
		this.initOK();
	}

	public Boolean checkUserTenantContrast(Long userId) {
		List l=getList("from "+getTableName()+" where userId="+userId,null);
		if(l!=null && l.size()>0){
			return true;
		}else{
			return false;
		}
	}

	public List<TenantEntity> getUserTenantContrast(Long id) {
		List<UserTenantEntity> l=getList("from "+getTableName()+" where userId="+id,null);
		if(l==null || l.size()==0)
			return null;
		ITenantService ts=(ITenantService) ServiceProxyFactory.getServiceProxy(TenantServiceImpl.class);
		List<TenantEntity> rtnList=new ArrayList<TenantEntity>();
		for(UserTenantEntity ut:l){
			TenantEntity t=ts.getEntityById(ut.getTenantId());
			if(t!=null && t.getStatus().equals(TenantEntity.TenantStatus_passed)){
				rtnList.add(t);
			}
		}
		return rtnList;
	}

	public UserTenantEntity getUserTenantContrastByTenantId(Long tenantId) {
		List<UserTenantEntity> utl=getList("from "+getTableName()+" where tenantId="+tenantId,null);
		if(utl==null || utl.size()==0)
			return null;
		return utl.get(0);
	}



}
