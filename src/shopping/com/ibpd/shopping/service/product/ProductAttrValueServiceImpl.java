package com.ibpd.shopping.service.product;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.ProductAttrValueEntity;
@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends BaseServiceImpl<ProductAttrValueEntity> implements IProductAttrValueService {
	public ProductAttrValueServiceImpl(){
		super();
		this.tableName="ProductAttrValueEntity";
		this.currentClass=ProductAttrValueEntity.class;
		this.initOK();
	}

	public List<ProductAttrValueEntity> getListByProductId(Long productId) {
		return getList("from "+getTableName()+" where productId="+productId,null);
	}

	public List<ProductAttrValueEntity> getAttributeLinkList(Long attributeId,
			Long productId) {
		return getList("from "+getTableName()+" where attrId="+attributeId+" and productId="+productId,null);
	}
}
