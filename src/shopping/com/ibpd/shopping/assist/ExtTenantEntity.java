package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.ibpd.shopping.entity.ProductEntity;
import com.ibpd.shopping.entity.TenantEntity;

public class ExtTenantEntity extends TenantEntity {

	private List<ProductEntity> productList=new ArrayList<ProductEntity>();

	public void setProductList(List<ProductEntity> productList) {
		this.productList = productList;
	}

	public List<ProductEntity> getProductList() {
		return productList;
	}
	public ExtTenantEntity(TenantEntity tenant){
		try {
			InterfaceUtil.swap(tenant, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addProduct(ProductEntity pe){
		this.productList.add(pe);
	}
}
