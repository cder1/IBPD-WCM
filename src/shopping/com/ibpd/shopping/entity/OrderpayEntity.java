package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_orderpay")
public class OrderpayEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String orderpay_paystatus_y = "y";//支付成功
	public static final String orderpay_paystatus_n = "n";//未支付成�?
	
	public static final String orderpay_paymethod_alipayescow = "alipayescow";//支付宝纯担保交易接口

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_orderid",length=11,nullable=true)
	private String orderid;
	@Column(name="f_paystatus",length=2,nullable=true)
	private String paystatus="n";
	@Column(name="f_payamount",nullable=true)
	private Double payamount;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createtime",nullable=true)
	private Date createtime;
	@Column(name="f_paymethod",length=22,nullable=true)
	private String paymethod;
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_confirmdate",nullable=true)
	private Date confirmdate;
	@Column(name="f_confirmuser",length=11,nullable=true)
	private String confirmuser;
	@Column(name="f_remark",length=2000,nullable=true)
	private String remark;
	@Column(name="f_tradeNo",length=45,nullable=true)
	private String tradeNo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getPaystatus() {
		return paystatus;
	}
	public void setPaystatus(String paystatus) {
		this.paystatus = paystatus;
	}
	public Double getPayamount() {
		return payamount;
	}
	public void setPayamount(Double payamount) {
		this.payamount = payamount;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getPaymethod() {
		return paymethod;
	}
	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}
	public Date getConfirmdate() {
		return confirmdate;
	}
	public void setConfirmdate(Date confirmdate) {
		this.confirmdate = confirmdate;
	}
	public String getConfirmuser() {
		return confirmuser;
	}
	public void setConfirmuser(String confirmuser) {
		this.confirmuser = confirmuser;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	@Override
	public String toString() {
		JSONArray json=JSONArray.fromObject(this);
		return json.toString();
	}
	
}
