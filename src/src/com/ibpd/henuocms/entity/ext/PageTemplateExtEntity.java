package com.ibpd.henuocms.entity.ext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
/**
 * 页面模版扩展类
 * @author mg by qq:349070443
 *
 */
public class PageTemplateExtEntity extends PageTemplateEntity {

	private String defaultStyleTemplateName=" ";
	private String subSiteName=" ";
	private String nodeName=" ";
	private StyleTemplateEntity styleTemplate=null;
	private PageTemplateEntity pageTemplate=null;

	public void setDefaultStyleTemplateName(String defaultStyleTemplateName) {
		this.defaultStyleTemplateName = defaultStyleTemplateName;
	}

	public String getDefaultStyleTemplateName() {
		return defaultStyleTemplateName;
	}

	public void setStyleTemplate(StyleTemplateEntity styleTemplate) {
		this.styleTemplate = styleTemplate;
		if(this.styleTemplate==null)
			return;
		this.setDefaultStyleTemplateName(this.styleTemplate.getTitle());
	}

	public StyleTemplateEntity getStyleTemplate() {
		return styleTemplate;
	}

	public void setPageTemplate(PageTemplateEntity pageTemplate) {
		this.pageTemplate = pageTemplate;
		if(pageTemplate!=null){
			try {
				setValue(pageTemplate);
//				ISiteService siteService=(ISiteService)ServoceProxyFactory(SiteServiceImpl.class);
//				SiteEntity site=siteService.getEntityById(pageTemplate.getSubSiteId());
//				if(site!=null)
//					this.setSubSiteName(site.getName());
				//等站点的相关模块做完之后，把上面的部分放开
				//下面这段暂时是用，完了删除
				this.setSubSiteName("和诺商业集团有限公司");
				INodeService nodeService=(INodeService) ServiceProxyFactory.getServiceProxy(NodeServiceImpl.class);
				NodeEntity ne=nodeService.getEntityById(pageTemplate.getNodeId());
				if(ne!=null)
					this.setNodeName(ne.getText());
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public PageTemplateEntity getPageTemplate() {
		return pageTemplate;
	}
	private void setValue(PageTemplateEntity pt) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method[] mts=pt.getClass().getMethods();
		for(Method mt:mts){
			String  mtName=mt.getName();
			if(mtName.substring(0,3).equals("get")){
				String tName="set"+mtName.substring(3);
				if(existMethod(tName)){
					if(tName.equals("setId")){
//						IbpdLogger.getLogger(this.getClass()).info();
					}
					Method mthod=this.getClass().getMethod(tName, mt.getReturnType());
					mthod.invoke(this, mt.invoke(pt, null));
				}
			}
		}
	}
	private Boolean existMethod(String mtName){
		Method[] mt=this.getClass().getMethods();
		for(Method m:mt){
			if(m.getName().equals(mtName)){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "PageTemplateExtEntity [defaultStyleTemplateName="
				+ defaultStyleTemplateName + ", pageTemplate=" + pageTemplate
				+ ", styleTemplate=" + styleTemplate
				+ ", getDefaultStyleTemplateName()="
				+ getDefaultStyleTemplateName() + ", getPageTemplate()="
				+ getPageTemplate() + ", getStyleTemplate()="
				+ getStyleTemplate() + ", getCreateDate()=" + getCreateDate()
				+ ", getDefaultStyleTemplateId()="
				+ getDefaultStyleTemplateId() + ", getEnableCopyPageContent()="
				+ getEnableCopyPageContent() + ", getFolderSize()="
				+ getFolderSize() + ", getGroup()=" + getGroup() + ", getId()="
				+ getId() + ", getIsDefault()=" + getIsDefault()
				+ ", getLastUpdateDate()=" + getLastUpdateDate()
				+ ", getNodeId()=" + getNodeId() + ", getOrder()=" + getOrder()
				+ ", getRemark()=" + getRemark() + ", getSize()=" + getSize()
				+ ", getSubSiteId()=" + getSubSiteId()
				+ ", getTemplateFilePath()=" + getTemplateFilePath()
				+ ", getTitle()=" + getTitle() + ", getType()=" + getType()
				+ ", getUseMouseRightKey()=" + getUseMouseRightKey() + "]";
	}

	public void setSubSiteName(String subSiteName) {
		this.subSiteName = subSiteName;
	}

	public String getSubSiteName() {
		return subSiteName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeName() {
		return nodeName;
	}

}
