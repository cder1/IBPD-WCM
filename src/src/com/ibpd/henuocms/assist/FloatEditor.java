package com.ibpd.henuocms.assist;
import org.springframework.beans.propertyeditors.PropertiesEditor;
/**
 * spring的自定义prop处理器
 * @author mg by qq:349070443
 *
 */
public class FloatEditor extends PropertiesEditor {  
    @Override  
    public void setAsText(String text) throws IllegalArgumentException {  
        if (text == null || text.equals("")) {  
            text = "0";  
        }  
        setValue(Float.parseFloat(text));  
    }  
  
    @Override  
    public String getAsText() {  
        return getValue().toString();  
    }  
}  