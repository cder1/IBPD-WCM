package com.ibpd.henuocms.service.paper;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.PaperEntity;

public interface IPaperService extends IBaseService<PaperEntity> {

}
 