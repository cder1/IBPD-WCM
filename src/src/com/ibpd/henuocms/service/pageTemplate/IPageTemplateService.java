package com.ibpd.henuocms.service.pageTemplate;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.ext.PageTemplateExtEntity;

public interface IPageTemplateService extends IBaseService<PageTemplateEntity> {

	void init();
	/**
	 * 获取模板列表,<br />
	 * 这里的取值顺序 ：如果nodeId有值，就优先取node下的模板，否则才去site下的模板，
	 * @param siteIds
	 * @param NodeId 
	 * @param pageSize
	 * @param pageIndex
	 * @param orderType
	 * @param filterString
	 * @return
	 */
	List<PageTemplateExtEntity> getList(Long[] siteIds,Long NodeId,Integer pageSize,Integer pageIndex,String orderType,String filterString);
	Long getRowCount(Long[] siteIds,Long nodeId,String filterString);
	Boolean checkPageTempIsUsed(Long id);
	PageTemplateEntity getDefaultPageTemplate(Integer type);
	void setStyleTempateId(Long pageTempId,Long styleTempId);
	void savePageTemplate(PageTemplateEntity pt);
}