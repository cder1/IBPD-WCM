/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins += (config.extraPlugins ? ',multiimage' : 'multiimage');
	var pathName = window.document.location.pathname;
    //获取带"/"的项目名，如：/uimcardprj
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    config.filebrowserImageUploadUrl = projectName+'/doCkUpload.do?type=image&nodeId='+nodeId+'&contentId='+contentId; 
	config.filebrowserFlashUploadUrl =projectName+'/doCkUpload.do?type=flash&nodeId='+nodeId+'&contentId='+contentId; 
	config.filebrowserUploadUrl =projectName+'/doCkUpload.do?type=file&nodeId='+nodeId+'&contentId='+contentId; 
};
