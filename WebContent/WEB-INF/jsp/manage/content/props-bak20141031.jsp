<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>属性配置</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:80px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr td input {
			width:130px;
		}
		.baseInfo tr td select {
			width:130px;
		}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   <div region="east" hide="true" split="false"  style="width:230px;" id="west">
		<div id="propPanel" class="easyui-accordion" fit="true" border="false">
		<input type="hidden" id="contentId" value="${entity.id }"/>
			<div title="基本信息">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
			<tr>
				<td class="tit">state</td>
				<td class="val">
					<input type="text" mthod="smt" name="state" value="${entity.state }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">title</td>
				<td class="val">
					<input type="text" mthod="smt" name="title" value="${entity.title }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">previewUrl</td>
				<td class="val">
					<input type="text" mthod="smt" name="previewUrl" value="${entity.previewUrl }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">description</td>
				<td class="val">
					<input type="text" mthod="smt" name="description" value="${entity.description }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">textKeywords</td>
				<td class="val">
					<input type="text" mthod="smt" name="textKeywords" value="${entity.textKeywords }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">redirectUrl</td>
				<td class="val">
					<input type="text" mthod="smt" name="isRedirectUrl" value="${entity.redirectUrl }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">aboutKeyword</td>
				<td class="val">
					<input type="text" mthod="smt" name="aboutKeyword" value="${entity.aboutKeyword }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">isRecommend</td>
				<td class="val">
					<select mthod="smt" name="isRecommend" value="${entity.isRecommend }">
						<option value="true" <c:if test="${entity.isRecommend==true }">selected</c:if>>是</option>
						<option value="true" <c:if test="${entity.isRecommend==false }">selected</c:if>>是</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">nodeId</td>
				<td class="val">
					<input type="text" mthod="smt" name="nodeId" value="${entity.nodeId }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">group</td>
				<td class="val">
					<input type="text" mthod="smt" name="group" value="${entity.group }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">source</td>
				<td class="val">
					<input type="text" mthod="smt" name="source" value="${entity.source }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">url</td>
				<td class="val">
					<input type="text" mthod="smt" name="url" value="${entity.url }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">author</td>
				<td class="val">
					<input type="text" mthod="smt" name="author" value="${entity.author }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">isTop</td>
				<td class="val">
					<select mthod="smt" name="isTop" value="${entity.isTop }">
						<option value="true" <c:if test="${entity.isTop==true }">selected</c:if>>是</option>
						<option value="true" <c:if test="${entity.isTop==false }">selected</c:if>>是</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">isParson</td>
				<td class="val">
					<select mthod="smt" name="isParson" value="${entity.isParson }">
						<option value="true" <c:if test="${entity.isParson==true }">selected</c:if>>是</option>
						<option value="true" <c:if test="${entity.isParson==false }">selected</c:if>>是</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="tit">order</td>
				<td class="val">
					<input type="text" mthod="smt" name="order" value="${entity.order }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">goodCount</td>
				<td class="val">
					<input type="text" mthod="smt" name="goodCount" value="${entity.goodCount }"/>
				</td>
			</tr>

				</table>
			</div>
				
			<div title="扩展字段">
				<table class="baseInfo" cellpadding="0" cellspacing="0">
			<tr>
				<td class="tit">custom1</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom1" value="${entity.custom1 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom2</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom2" value="${entity.custom2 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom3</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom3" value="${entity.custom3 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom4</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom4" value="${entity.custom4 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom5</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom5" value="${entity.custom5 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom6</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom6" value="${entity.custom6 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom7</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom7" value="${entity.custom7 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom8</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom8" value="${entity.custom8 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom9</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom9" value="${entity.custom9 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom10</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom10" value="${entity.custom10 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom11</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom11" value="${entity.custom11 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom12</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom12" value="${entity.custom12 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom13</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom13" value="${entity.custom13 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom14</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom14" value="${entity.custom14 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom15</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom15" value="${entity.custom15 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom16</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom16" value="${entity.custom16 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom17</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom17" value="${entity.custom17 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom18</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom18" value="${entity.custom18 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom19</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom19" value="${entity.custom19 }"/>
				</td>
			</tr>
			<tr>
				<td class="tit">custom20</td>
				<td class="val">
					<input type="text" mthod="smt" name="custom20" value="${entity.custom20 }"/>
				</td>
			</tr>				</table>			
			</div>
	    </div>
    </div>
	</div>
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		$("[mthod='smt']").bind("blur",function(e){
			var contentId=$("#contentId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).children("option:selected").attr("id");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/Content/saveProp.do",
				{id:contentId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
	});        
     </script>
     
	</body>
</html>
