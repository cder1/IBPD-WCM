<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/poshytip-1.2/src/tip-yellow/tip-yellow.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:500px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val *{
			margin:2 3 2 3;
			width:96%;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.validatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/extValidatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/poshytip-1.2/src/jquery.poshytip.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcore.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcalendar.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;"> 
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
			
				<table class="baseInfo" cellpadding="0" cellspacing="0">
					<tr>
						<tH class="tit"><div class="tittext">站点名称</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="siteName" value="${site.siteName }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">站点全称</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="fullName" value="${site.fullName }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">管理员</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="manageUser" value="${site.manageUser }"/>
							
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">域名</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="domainName" value="${site.domainName }"/>
							
							</span>
						</td>
					<tr>
					<!--tr>
						<th class="tit"><div class="tittext">站点开始生效时间</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="startDate" value="${site.startDate }"/>
							</span>
						</td>
					<tr>
					<tr>
						<th class="tit"><div class="tittext">站点关闭时间</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt"  name="endDate" value="${site.endDate }"/>
							</span>
						</td>
					<tr-->
						<th class="tit"><div class="tittext">描述</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="description" value="${site.description }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">搜索关键字</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="keywords" value="${site.keywords }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">排序</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="order" value="${site.order }"/>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">站点页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="sitePageTemplateId">
								<option value="-1" <c:if test="${site.sitePageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${sitePageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.sitePageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">站点样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="siteStyleTemplateId">
								<option value="-1" <c:if test="${site.siteStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${siteStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.siteStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">栏目页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="nodePageTemplateId">
								<option value="-1" <c:if test="${site.nodePageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${nodePageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.nodePageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">栏目样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="nodeStyleTemplateId">
								<option value="-1" <c:if test="${site.nodeStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${nodeStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.nodeStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">内容页面模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="contentpageTemplateId">
								<option value="-1" <c:if test="${site.contentpageTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${contentpageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==site.contentpageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">内容样式模板</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="contentStyleTemplateId">
								<option value="-1" <c:if test="${site.contentStyleTemplateId==-1 }">selected</c:if>>使用默认设置</option>
								<c:forEach items="${contentStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==site.contentStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">背景音乐</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="bgMusic" value="${site.bgMusic }"/>
							</span>
							<input type="button" smt="fileSelecter" name="bgMusic_fileSelecter" value="..."/>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">logo标志</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="logo" value="${site.logo }"/>
							</span>
							<input type="button" smt="fileSelecter" name="logo_fileSelecter" value="..."/>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">图标</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="icon" value="${site.icon }"/>
							</span>
							<input type="button" smt="fileSelecter" name="icon_fileSelecter" value="..."/>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">横幅图片</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="banner" value="${site.banner }"/>
							</span>
							<input type="button" smt="fileSelecter" name="banner_fileSelecter" value="..."/>
						</td>
					</tr>
					<tr>
						<th class="tit"><div class="tittext">版权信息</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="copyright" value="${site.copyright }"/>
							</span>
						</td>
					</tr>
				</table>
			</div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
		bindEvents();
	});
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){showSelectFile(e,-1);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
	function showSelectFile(e,nodeId){
		curImageId=$(e.target).attr("name").split("_")[0];
		window.open(path+'/Manage/FileSelecter/index.do?siteId='+nodeId+'&filter='+curImageId,'文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(s!=""){
			s=s.substring(0,s.length-1);
		}
		if($("input[name='"+f+"_fileAppend']:checked").length>0){
			$("input[name='"+f+"']").val($("input[name='"+f+"']").val()+";"+s);
		}else{
			$("input[name='"+f+"']").val(s);
		}
		//$("input[name='"+f+"']").val(s);
		$("input[name='"+f+"']").focus();
	};
	
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	function submit(){
		var _s_tmp=$("[data-options]");
		var flog=true;
		for(i=0;i<_s_tmp.size();i++){
			if(!$(_s_tmp[i]).validatebox("isValid")){
				flog=false;
				break;
			}
		} 
		if(!flog){
			return false;
		}
		_s_tmp=$("[mthod='smt']");
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			if(_s_tmp[i].tagName=="SELECT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";//.selectedOptions[0].value+"',";
			}else if(_s_tmp[i].tagName=="INPUT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";
			}else{
			}
		} 
		params="[{"+params+"tmp:''}]";
			$.ajax({
				 type: "POST",
				 url: path+"/Manage/SubSite/doAdd.do",
				 data: eval(params)[0],
				 dataType: "text",
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							rtn=true;
						}else{
							alert(t.msg);
						}
					}
				}
			 });
		return true;
	}
</script>